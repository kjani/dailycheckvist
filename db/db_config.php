<?php 

/**
 * Database config options
 * */

    $db_config['db.options']['driver']      =   'pdo_mysql';
    $db_config['db.options']['dbname']      =   'dailycheckvist';
    $db_config['db.options']['host']        =   'localhost';
    $db_config['db.options']['user']        =   null;
    $db_config['db.options']['password']    =   null;
    $db_config['db.options']['port']        =   '3306';
