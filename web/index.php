<?php 
    require_once __DIR__.'/../vendor/autoload.php';
    use Guzzle\Http\Client as GuzzleClient;
    use Silex\Provider\FormServiceProvider;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;

    $app = new Silex\Application();

    /**
     * Regsiter Services
     * */
        //Twig
        $app->register(new Silex\Provider\TwigServiceProvider(), array(
            'twig.path' => __DIR__.'/../views',
            'twig.options' => array(
                    'debug' =>  true
                )
        ));

        // Form related stuff
        $app->register(new FormServiceProvider());
        $app->register(new Silex\Provider\ValidatorServiceProvider());
        $app->register(new Silex\Provider\TranslationServiceProvider(), array(
            'translator.messages' => array(),
        ));

        // Url
        $app->register(new Silex\Provider\UrlGeneratorServiceProvider());


        //Database - Pull up config. Register service
        if ( file_exists( dirname( __FILE__ ) . '/../db/db_config_local.php' ) ) {
            include( dirname( __FILE__ ) . '/../db/db_config_local.php' );
        } else {
            include( dirname( __FILE__ ) . '/../db/db_config.php' );
        }
        $app->register(new Silex\Provider\DoctrineServiceProvider(), $db_config);

        $app['debug'] = true;

    /**
     * Routes
     * */
        // Main
        $app->match('/', function(Request $request) use ($app) {

            //Generate form specs using symfony forms
            $formBuilder = $app['form.factory']->createBuilder('form');
            $formBuilder->add('username','email',[
                'attr' => [
                    'class' => 'form-control', 
                    'placeholder' => 'Enter the email you use to login to checkvist'
                ],
                'constraints' => new Assert\Email()
            ]);
            $formBuilder->add('api_key', 'text', [
                'attr' => ['class' => 'form-control', 'placeholder' => 'Enter your Checkvist API Key']]);
            $formBuilder->add('date_format', 'choice', [
                'choices' => [
                        'n/j/Y' => date('n/j/Y'),
                        'j/n/Y' => date('j/n/Y'),
                        'j M Y' => date('j M Y'),
                        'M j Y' => date('M j Y'),
                    ],  
                'attr' => ['class' => 'form-control'],
                'empty_value' => 'Pick a date format',
                'constraints' => new Assert\Choice(['n/j/Y', 'j/n/Y', 'j M Y', 'M j Y'])
            ]);
            $formBuilder->add('list_type', 'choice', [
                'choices' => ['new' => 'Create a New List', 'existing' => 'Use one of my existing lists'],
                'expanded' => true,
                'multiple' => false,
                'constraints' => new Assert\Choice(['new', 'existing'])
            ]);
            $formBuilder->add('existing_lists', 'choice', [
                'choices' => [],
                'expanded' => false,
                'multiple' => false,
                'attr' => ['class' => 'form-control'],
                'empty_value' => 'Choose one of your existing lists',
                'required' => false
            ]);
            $formBuilder->add('timezone', 'timezone', [
                'attr' => ['class' => 'form-control'], 
                'empty_value' => 'Select your timezone'
            ]);
            $form = $formBuilder->getForm();
            $form->handleRequest($request);


            //If form is being submitted, check if valid, if not, continue displaying with errors
            if ($form->isValid()) 
            {
                $formData = $form->getData();

                //Get guzzle token
                try {       
                    $guzzleClient = new GuzzleClient('http://checkvist.com');
                    $request = $guzzleClient->post('/auth/login.json', array(), array(
                        'username' => $formData['username'],
                        'remote_key' => $formData['api_key']
                    ));
                    $response = $request->send();
                    $checkvistToken = $response->getBody();
                    $checkvistToken = str_replace('"', '', $checkvistToken);
                } catch (Guzzle\Http\Exception\BadResponseException $e) {
                    $data['response'] = $e->getMessage();
                    return $data['response'];
                }

                //Determine if it's a new or existing list.
                //If new, create list and get list id
                if ($formData['list_type'] == 'new') 
                {
                    try {       
                        $guzzleClient = new GuzzleClient('http://checkvist.com');
                        $request = $guzzleClient->post('/checklists.json', array(), array(
                            'token' => $checkvistToken,
                            'checklist[name]' => date($formData['date_format'])
                        ));
                        $response = $request->send();
                        $data['response'] = $response->getBody();
                    } catch (Guzzle\Http\Exception\BadResponseException $e) {
                        $data['response'] = $e->getMessage();
                    }                    
                    return $data['response'];
                }
                
/*                var_dump($formData);
                echo $data['response'];
                return '';*/


                //Update list name with date format

                //Store in db for cron jobs
            }

            $data['form'] = $form->createView();
            return $app['twig']->render('form.twig', $data);
        });

        //Check api credentials.
        $app->post('/verify_api', function() use ($app) {

            //Check inputs to make sure everything looks good
            if (filter_var($_POST['username'], FILTER_VALIDATE_EMAIL) == false) 
            {
                return 'Error: Please enter a valid email';
            }

            $username = $_POST['username'];
            $remote_key = filter_var($_POST['remote_key'], FILTER_SANITIZE_STRING);
            
            //Connect to checkvist and get token
            try {       
                $guzzleClient = new GuzzleClient('http://checkvist.com');
                $request = $guzzleClient->post('/auth/login.json', array(), array(
                    'username' => $username,
                    'remote_key' => $remote_key
                ));
                $response = $request->send();
                $data['response'] = $response->getBody();
            } catch (Guzzle\Http\Exception\BadResponseException $e) {
                $data['response'] = "Error: Check your email and API Key";
            }

            return $data['response'];
        });

        //Get all non-archived lists for user if modifying existing lists
        $app->post('/get_checklists', function() use ($app) {

            //Check inputs to make sure everything looks good
            if (filter_var($_POST['username'], FILTER_VALIDATE_EMAIL) == false) 
            {
                return 'Error: Please enter a valid email';
            }

            $username = $_POST['username'];
            $remote_key = filter_var($_POST['remote_key'], FILTER_SANITIZE_STRING);

            //Connect to checkvist and get lists
            try {       
                $guzzleClient = new GuzzleClient('http://checkvist.com');
                $request = $guzzleClient->get('/checklists.json')->setAuth($username, $remote_key);
                $response = $request->send();
                $data['response'] = $response->getBody();
            } catch (Guzzle\Http\Exception\BadResponseException $e) {
                $data['response'] = $e->getMessage();
            }

            return $data['response'];
        });

    $app->run();