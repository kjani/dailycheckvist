var checkvistToken;
var usersLists = null;

$(function(){

    //Make sure credentials are good.
    $("#check-api-btn").click(function(){
        
        $("#check-api-loader").show();
        $("#check-api-error").hide();

        $.ajax({
            type: "POST",
            url: "index.php/verify_api",
            data: { username: $("input[name='form[username]'").val(), remote_key: $("input[name='form[api_key]'").val() }
            })
            .done(function( msg ) {
                $("#check-api-loader").hide();

                if (msg.substring(0,5) == "Error") {
                    $("#check-api-error").html(msg);
                    $("#check-api-error").show();
                } else {
                    $("#check-api-loader").hide();
                    $("#check-api").hide();
                    $("input[name='form[username]'").attr('readonly', true);
                    $("input[name='form[api_key]'").attr('readonly', true);
                    $("#checkvist-related").show();
                    checkvistToken = msg;
                }
            }
        );
    
        return false;
    });

    //Reload page if reset is clicked
    $("#reset-btn").click(function(){
        location.reload();
        return false;
    });

    //Retrieve user's lists if modifying an existing list
    //Disable lists option if new list is selected.
    $("input[name='form[list_type]'").change(function() {
        if ($(this).val() == 'existing' && usersLists == null) {
            
            $("#user-lists-loader").show();

            $.ajax({
                type: "POST",
                url: "index.php/get_checklists",
                data: { username: $("input[name='form[username]'").val(), remote_key: $("input[name='form[api_key]'").val() },
                })
                .done(function( msg ) {
                    if (msg.substring(0,5) == "Error") {
                        $("#user-lists-error").html(msg);
                        $("#user-lists-error").hide();
                    } else {
                        usersLists = $.parseJSON( msg );
                        $.each(usersLists, function(key, value){
                            $("#form_existing_lists").append("<option value='"+value.id+"'>"+value.name+"</option>");
                            $("#existing-lists").show();
                        });
                        $("#user-lists-loader").hide();
                    }
                }
            );
        }

        if ($(this).val() == 'existing' && usersLists != null) {
            $("#existing-lists").show();
        }

        if ($(this).val() == 'new') {
            $("#existing-lists").hide();
        }
    });
    
});